var menuLinks = document.querySelectorAll("ul.navbar-nav a");

menuLinks.forEach(function(link) {
    link.addEventListener("click", function(e) {
        e.preventDefault(); 

        var targetId = this.getAttribute("href");

        document.querySelector(targetId).scrollIntoView({
            behavior: "smooth"
        });
    });
});